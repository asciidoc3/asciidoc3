= Test

== First Paragraph ==
Here comes something, nobody cares about this stuff. And now *bold*, and this is _italic_, and the last +monospaced+. After this (hidden +) +
begins a new line.
The german umlauts and two other non-ascii glyphs / letters: äöüÄÖÜß € (euro) ¢ (cent). 

== Second Paragraph / Images
This sample comes with an image. The file name paths are relative to the location of the referring document:
image:redsquare.jpg[alt="First Image",height="32",link="images/tiger.png"] The "caption" in brackets corresponds to HTML alt="First Image" and is not seen in the browser. The image is rendered inline; this is perhaps not preferred footnote:[or is it?], we have a footnote here. The same image image:redsquare.jpg[height=50,alt="testimage redsquare"] in the same line.

Another snippet

image::redsquare.jpg[align="left"]

This image doesn't exist: image:bluesquare.jpg[alt="bluesquare"]


New line and the image at the beginningfootnote:[footnote without space] of the line.

New line:

image::redsquare.jpg["caption",align="center"]

[NOTE]
You see an icon here?!

If not, did you provide [green]#-a icons# like so? 'asciidoc3 -a toc -n -a icons doc/test.txt' +
We see the image at the center of the line. At the end a link: Visit the https://www.asciidoc3.org[home of asciidoc3]! + 
_END_
