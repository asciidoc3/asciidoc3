#!/usr/bin/env python3

"""Cleanup test artifacts [beta]

   Remove after running testasciidoc3.py or testa2x3.py all
   created files to prepare for a new run.
   Usage:
   python3 ./cleanup_test_artifacts.py
   (c) 2025 by Berthold Gehrke <berthold.gehrke@gmail.com>
   License: GNU Affero General Public License v3 or higher (AGPLv3+)"""

import os
import shutil

if os.path.exists('./data/images/'):
    shutil.rmtree('./data/images/')
    print("remove ./data/images/")

# remove older output in ../../data/ (files ending with '.html', '.xml', or '.pdf')
for output_file in os.listdir("./data/"):
    if output_file.endswith('.xml') or output_file.endswith('.html') \
        or output_file.endswith('.pdf') or '_fop' in output_file:
        if output_file != 'test-docbook45.xml' and output_file != 'test-docbook51.xml':
            os.remove("./data/"+output_file)
            print("remove", output_file)

# remove .ly and .abc in 'images'
for output_file in os.listdir("../images/"):
    if output_file.endswith('.ly') or output_file.endswith('.abc'):
        os.remove("../images/"+output_file)
        print("removed:", "../images/"+output_file)

print()
# TODO: be careful about a possible symlink attack
if os.path.exists('./data/a2x3testdata/'):
    shutil.rmtree('./data/a2x3testdata/')
    print("remove ./data/a2x3testdata/")

artifacts_to_remove = (
                'latex1.md5',
                'latex1.png',
                'latex-filter__1.md5',
                'latex-filter__1.png',
                'latex-filter__2.md5',
                'latex-filter__2.png',
                'latex-filter__3.md5',
                'latex-filter__3.png',
                'music1.md5',
                'music1.png',
                'music2.md5',
                'music2.png',
                'open-block-test__1.md5',
                'open-block-test__1.png',
                'open-block-test__3.md5',
                'open-block-test__3.png',
                'slidy-example__1.md5',
                'slidy-example__1.png',
                'graphviz1.png',
                'graphviz2.png',
                'open-block-test__1.png',
                'open-block-test__2.png',
                'open-block-test__3.png',
                'slidy-example__1.png',
                'open-block-test__2.md5',
                'open-block-test_fop__1.png',
                'open-block-test_fop__2.png',
                'open-block-test_fop__3.png',
                'open-block-test_fop__1.md5',
                'open-block-test_fop__3.md5',
                )
for item in artifacts_to_remove:
    if os.path.exists('../images/' + item):
        os.remove('../images/' + item)
        print("remove in dir '../images/ ->' ", item)

for item in artifacts_to_remove:
    if os.path.exists('./' + item):
        os.remove('./' + item)
        print("remove in dir './ ->' ", item)

for item in artifacts_to_remove:
    if os.path.exists('./data/' + item):
        os.remove('./data/' + item)
        print("remove in dir './data/-> ' ", item)

# remove .tex in images
for output_file in os.listdir("../images/"):
    if output_file.endswith('.tex'):
        os.remove("../images/"+output_file)
        print("removed:", "../images/"+output_file)

# remove 'temp...' in images
for output_file in os.listdir("../images/"):
    if output_file.startswith('tmp'):
        os.remove("../images/"+output_file)
        print("removed:", "../images/"+output_file)

if os.path.exists('../doc/test.html'):
    os.remove('../doc/test.html')
