#!/usr/bin/env python3

"""
Generate a static »Table of contents« (TOC) for AsciiDoc3 HTML-files
without Javascript

[release-candidate]
You see here a somewhat to detailed description --- will be shortened
when we reach the final stage.
User can start this via option »htmltoc«.

Step 1:
asciidoc3.py reads mytext.ad3 and writes html output to mytext.html
into a tempory spooled file

Step 2:
Function/program »asciidoc3_toc_generator« reads the just generated
file, which looks like this

...
<body class="book">
<div id="header">
<h1>Test</h1>
<div id="toc">
  <div id="toctitle">Inhaltsverzeichnis</div>
  <noscript><p><b>JavaScript must be enabled in your browser
  to display the table of contents.</b></p></noscript>
</div>
</div>
<div id="content">
<div class="sect1">
<h2 id="_first_paragraph">1. First Paragraph</h2>
<div class="sectionbody">
<div class="paragraph"><p>Here comes something, nobody cares about
this stuff. And now <strong>bold</strong>, and this is <em>italic</em>,
and the last <span class="monospaced">monospaced</span>.
After this (hidden +)<br> begins a new line.
The german umlauts and two other non-ascii glyphs / letters:
äöüÄÖÜß € (euro) ¢ (cent).</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_second_paragraph">2. Second Paragraph</h2>
<div class="sectionbody">
...

Step 3:
»asciidoc3_toc_generator« searches for the headlines:

...
<h1>Test</h1>
<h2 id="_first_paragraph">1. First Paragraph</h2>
<h2 id="_second_paragraph">2. Second Paragraph</h2>
...

and builds a list of this lines

Step 4:
Insert TOC <div id="toctitle">Table of Content</div> to
mytext.html and write this as the final putput
(backup the html without TOC)

COPYING
    Copyright (C) 2025 by Berthold Gehrke.
    Free use of this software is granted under the terms
    of the AGPLv3 or higher."""


from os.path import splitext
import re
# tempfile only in final stage
# import tempfile
# Debug only:
# import pprint

VERSION = '4.0.1'


def gen_toc(file_to_toc):
    print("file_to_toc", file_to_toc)
    with open(file_to_toc, "r", encoding='utf-8', newline="") as fileinput:
        a = fileinput.read()

    toclist = list()
    # h1 looks like this:
    # <h1 id="_asciidoc3_user_guide">AsciiDoc3 User Guide</h1>
    # if there is no "id" we don't need a tocentry
    for levelnumber in range(1, 6):
        levelstring = str(levelnumber)
        regexheadline = re.compile(
            r'<h'+levelstring+r' id="(.*?)">(.*?)</h'+levelstring+r'>',
            re.IGNORECASE)
        result = regexheadline.finditer(a)
        for item in result:
            toclist.append((item.start(), levelstring,
                            item.group(1), item.group(2)))
        # sort toclist by first element of tuple (= startpoint of match)
        toclist.sort(key=lambda x: x[0])

    # pprint.pprint(toclist)
    # toclist looks like this - pprint.pprint(toclist):
    # ...
    # (149603, '4', '_block_identifier', '20.2.1. Block Identifier'),
    # (150244, '4', 'X49', '20.2.2. Images'),
    # (153133, '4', 'X25', '20.2.3. Comment Lines'),
    # (154255, '3', '_system_macros', '20.3. System Macros'),
    # ...
    tocad3 = '<div><br>'
    for item in toclist:
        tocad3 += '&nbsp; ' * (2*int(item[1]))
        tocentry = '<a href="#'+item[2]+'">'+item[3]+'</a>'
        tocad3 += tocentry+'<br>\n'
    tocad3 += '<div>'
    # pprint.pprint(tocad3)

    """
    <div id="toc">
      <div id="toctitle">Table of Contents</div>
      <noscript><p><b>JavaScript must be enabled in your \
      browser to display the table of contents.</b></p></noscript>
    </div>"""
    a = re.sub(
        r"""<div id="toctitle">(.*?)</div>.*?
        <noscript><p><b>.*?</b></p></noscript>""",
        r'<div id="toctitle">\g<1></div>'+tocad3+'\n',
        a, count=1, flags=re.IGNORECASE | re.DOTALL | re.VERBOSE)

    # write new generated file to disc
    with open(splitext(file_to_toc)[0], "w",
              encoding='utf-8', newline="") as fileoutput:
        fileoutput.write(a)
