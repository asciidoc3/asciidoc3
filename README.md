# asciidoc3
## Text based document generation using Python

AsciiDoc3 is a text document format for writing notes, documentation, articles, books,
ebooks, slideshows, web pages, man pages and blogs. AsciiDoc3 files can be translated
to many formats including HTML, PDF, EPUB, man page, and DocBook markup (at your choice v4.5 or v5.1).
AsciiDoc3 is highly configurable: both the AsciiDoc3 source file syntax and the backend
output markups (which can be almost any type of SGML/XML markup) can be customized and extended by the user.

## Prerequisites
AsciiDoc3 is written in 100% pure Python. So you need a Python interpreter (version 3.9 or later)
to execute asciidoc3.py.  
An 'onefile' solution with an embedded Python is available, so you don't need an installed Python interpreter in many use cases any more.

## Obtaining AsciiDoc3
deb rpm docker tarball generic installer zip PyPI (GNU/Linux and Windows) Pelican Plugin  
See documentation and installation instructions on the AsciiDoc3 website https://asciidoc3.org/

## Tools
AsciiDoc3 was tested on Debian Siduction, Ubuntu 24.04 LTS (Noble Numbat), GhostBSD, openSUSE Tumbleweed,
Windows 10/11, and other platforms (Python 3.9 to 3.12).

## Dependencies
AsciiDoc3 comes without dependencies (aside Python3, of course, if you want to run 'everything'). To exploit all features of AsciiDoc3, you need - where necessary - DocBook XSL Stylesheets, xsltproc, w3m, dblatex, FOP, Pygments, graphviz ...
You'll find these packages in the repos of your operating system - to avoid this hassle use Docker.

## Annotations
This AsciiDoc3 repo's history was rewritten (2023-21-1)! 
Kindly fork again; you'll find the Python2 history here: https://github.com/asciidoc3, look for commit message 'Rename asciidoc.conf to asciidoc3.conf'. 
AsciiDoc3 is now licensed under the GNU Affero General Public License Version 3 or later 
The closing GPLv2+ version (2023-20-1) is tagged with 'v3.2.3a=GPLv2+_vs_AGPLv3+'  
Default branch was renamed to 'main' (2022-21-12)  

## Copying
Copyright © 2018-2025 Berthold Gehrke <berthold.gehrke@gmail.com> for AsciiDoc3 (Python3)  
[Copyright © Stuart Rackham (and contributors) for AsciiDoc v8.6.9 (Python2; End-Of-Life since Jan. 2020)]  

Free use of this software is granted under the terms of the  
GNU Affero General Public License version 3 or later (AGPLv3+).
